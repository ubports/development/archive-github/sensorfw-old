Source: sensorfw-qt5
Section: utils
Priority: optional
Maintainer: UBports Developers <devs@ubports.com>
Build-Depends: debhelper-compat (= 12),
               android-headers,
               doxygen,
               graphviz,
               libgbinder-dev (>= 1.1.14),
               libglib2.0-dev,
               libglibutil-dev,
               libhardware-dev,
               libhybris-dev,
               libsystemd-dev,
               libudev-dev,
               pkg-config,
               qt5-qmake,
               qtbase5-dev,
Standards-Version: 3.7.3
Homepage: https://github.com/sailfishos/sensorfw
Vcs-Git: https://github.com/sailfishos/sensorfw.git
Vcs-Browser: https://github.com/sailfishos/sensorfw

Package: sensorfw-qt5
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Sensor framework daemon
 Provides sensor framework daemon and required libraries

Package: libsensorfw-qt5-plugins
Section: libs
Architecture: any
Depends: sensorfw-qt5 (= ${binary:Version}),
         udev,
         ${shlibs:Depends},
         ${misc:Depends},
Description: Sensor framework daemon and libraries plugins
 Platform plugins for sensorfw.

Package: libsensorfw-qt5-dev
Section: libdevel
Architecture: any
Depends: sensorfw-qt5 (= ${binary:Version}),
         qtbase5-dev,
         ${misc:Depends}
Description: Sensor framework daemon libraries development headers
 Development headers for sensor framework daemon and libraries.

Package: libsensorfw-qt5-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Description: API documentation for libsensorfw
 Doxygen-generated API documentation for sensorfw.

Package: sensorfw-qt5-tests
Section: misc
Architecture: any
Depends: sensorfw-qt5 (= ${binary:Version}),
         python3:any,
         ${shlibs:Depends},
         ${misc:Depends}
Description: Unit test cases for sensorfw
 Test cases and supporting binaries for testing sensorfw.

Package: libsensorfw-qt5-hybris
Section: libs
Architecture: any
Depends: sensorfw-qt5 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Description: Sensor framework hybris support
 Provides support for hybris Sensor framework backend.

Package: libsensorfw-qt5-hidl
Section: libs
Architecture: any
Depends: sensorfw-qt5 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Description: Sensor framework HIDL support
 Provides support for HIDL Sensor framework backend.
